package gpes.ifpb.edu.br.abpres.categorias.controller;

import java.util.List;

import javax.validation.Valid;

import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gpes.ifpb.edu.br.abpres.categorias.dto.CategoriaDTO;
import gpes.ifpb.edu.br.abpres.categorias.dto.CategoriaRetornoDTO;
import gpes.ifpb.edu.br.abpres.categorias.service.CategoriaService;

@RestController
@RequestMapping("/api/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaService categoriaService;
	
	@CrossOrigin
	@PostMapping()
	public ResponseEntity<CategoriaRetornoDTO> create(@Valid @RequestBody CategoriaDTO categoriaDto) {
		CategoriaRetornoDTO created = categoriaService.create(categoriaDto);
		return ResponseEntity.status(HttpStatus.CREATED).body(created); 
	}
	
	@CrossOrigin
	@GetMapping()
	public ResponseEntity<List<CategoriaRetornoDTO>> getAll() {
		return ResponseEntity.ok(categoriaService.getAll());
	}
	
	@CrossOrigin
	@GetMapping(value = "/{categoriaId}")
	public ResponseEntity<CategoriaRetornoDTO> getOne(@PathVariable Long categoriaId) {
		
		CategoriaRetornoDTO categoriaRetornoDto = categoriaService.getOne(categoriaId);
		
		if (categoriaRetornoDto != null)
			return ResponseEntity.ok(categoriaRetornoDto);
		
		throw new ResourceNotFoundException(categoriaId);
	}
	
	@CrossOrigin
	@PutMapping(value = "/{categoriaId}")
	public ResponseEntity<CategoriaRetornoDTO> update(@PathVariable Long categoriaId, @Valid @RequestBody CategoriaDTO categoriaDto) {
		CategoriaRetornoDTO updated = categoriaService.update(categoriaId, categoriaDto);
		return ResponseEntity.ok(updated);
	}
	
	@CrossOrigin
	@DeleteMapping(value = "/{categoriaId}")
	public ResponseEntity<CategoriaRetornoDTO> delete(@PathVariable Long categoriaId) {
		CategoriaRetornoDTO deleted = categoriaService.delete(categoriaId);
		return ResponseEntity.ok(deleted);
	}
}
