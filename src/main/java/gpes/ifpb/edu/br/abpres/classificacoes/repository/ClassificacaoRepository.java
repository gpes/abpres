package gpes.ifpb.edu.br.abpres.classificacoes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gpes.ifpb.edu.br.abpres.classificacoes.model.Classificacao;

public interface ClassificacaoRepository extends JpaRepository<Classificacao,Long> {

}
