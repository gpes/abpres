package gpes.ifpb.edu.br.abpres.usuarios.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "TB_USUARIO", uniqueConstraints=
@UniqueConstraint(columnNames={"email", "username"}))
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "id_usuario")
    private Long id;

    @NotNull
    @Size(max = 70)
    private String username;

    @NotNull
    @Email
    @Size(max = 100)
    private String email;

    @NotNull
    @Size(max = 128)
    private String senha;

    @NotNull
    private String tipo;

	public Usuario(final String username, final String email, final String senha, final Tipo tipo) {
        this.username = username;
        this.email = email;
        this.senha = senha;
        this.tipo = tipo.name();
    }

    public Usuario() {

    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(final String senha) {
        this.senha = senha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(final String tipo) {
        this.tipo = tipo;
    }


}
