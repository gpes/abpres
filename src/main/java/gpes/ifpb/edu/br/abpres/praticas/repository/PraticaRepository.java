package gpes.ifpb.edu.br.abpres.praticas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;

public interface PraticaRepository extends JpaRepository<Pratica, Long> {
	
}
