package gpes.ifpb.edu.br.abpres.usuarios.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import gpes.ifpb.edu.br.abpres.usuarios.model.Usuario;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Long> {

	public Optional<Usuario> findByUsernameOrEmail(String username, String email);

	public Optional<Usuario> findByUsername(String username);

}
