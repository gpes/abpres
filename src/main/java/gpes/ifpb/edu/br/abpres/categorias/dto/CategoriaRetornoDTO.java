package gpes.ifpb.edu.br.abpres.categorias.dto;

import java.util.List;

import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;
import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;

public class CategoriaRetornoDTO {
	
	private Long id;
	private String titulo;
	private List<Pratica> praticas;
	
	public CategoriaRetornoDTO() {
	}

	public CategoriaRetornoDTO(Long id, String titulo, List<Pratica> praticas) {
		this.id = id;
		this.titulo = titulo;
		this.praticas = praticas;
	}

	public CategoriaRetornoDTO(Categoria categoria) {
		this.id = categoria.getId();
		this.titulo = categoria.getTitulo();
		this.praticas = categoria.getPraticas();
	}

	public CategoriaRetornoDTO(CategoriaDTO categoriaDTO) {
		this.titulo = categoriaDTO.getTitulo();
		this.praticas = categoriaDTO.getPraticas();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public List<Pratica> getPraticas() {
		return praticas;
	}
	public void setPraticas(List<Pratica> praticas) {
		this.praticas = praticas;
	}
	
	public CategoriaDTO toCategoriaDto (Categoria categoria) {
		
		return new CategoriaDTO(categoria.getTitulo(),categoria.getPraticas());
	}
	
	public Categoria toCategoria (CategoriaDTO dtoCategoria) {
		return new Categoria(dtoCategoria.getTitulo(),dtoCategoria.getPraticas());
	}

}
