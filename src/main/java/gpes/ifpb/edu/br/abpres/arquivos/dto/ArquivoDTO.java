package gpes.ifpb.edu.br.abpres.arquivos.dto;

import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;
import gpes.ifpb.edu.br.abpres.classificacoes.model.Classificacao;
import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;

public class ArquivoDTO {

	private String nome;

	private String path;

	private String descricao;

	private Pratica pratica;

	private Classificacao classificacao;

	public ArquivoDTO() {
		
	}

	public ArquivoDTO(String nome, String path, String descricao, Pratica pratica, Classificacao classificacao) {
		
		this.nome = nome;
		this.path = path;
		this.descricao = descricao;
		this.pratica = pratica;
		this.classificacao = classificacao;
	}

	public ArquivoDTO(Arquivo arquivo) {
		
		this.nome = arquivo.getNome();
		this.path = arquivo.getPath();
		this.descricao = arquivo.getDescricao();
		this.pratica = arquivo.getPratica();
		this.classificacao = arquivo.getClassificacao();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Pratica getPratica() {
		return pratica;
	}

	public void setPratica(Pratica pratica) {
		this.pratica = pratica;
	}

	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}
	
	public Arquivo toArquivo(ArquivoDTO dtoArquivo) {
		return new Arquivo(dtoArquivo.getNome(),dtoArquivo.getPath(),dtoArquivo.getDescricao(),dtoArquivo.getPratica(),dtoArquivo.getClassificacao());
	}
	
	public ArquivoDTO toArquivoDto(Arquivo arquivo) {
		return new ArquivoDTO(arquivo.getNome(),arquivo.getPath(),arquivo.getDescricao(),arquivo.getPratica(),arquivo.getClassificacao());
	}
	

}
