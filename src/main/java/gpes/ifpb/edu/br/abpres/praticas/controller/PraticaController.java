package gpes.ifpb.edu.br.abpres.praticas.controller;

import java.util.List;

import javax.validation.Valid;

import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gpes.ifpb.edu.br.abpres.praticas.dto.PraticaDTO;
import gpes.ifpb.edu.br.abpres.praticas.dto.PraticaRetornoDTO;
import gpes.ifpb.edu.br.abpres.praticas.service.PraticaService;

@RestController
@RequestMapping("/api/praticas")
public class PraticaController {

	@Autowired
	private PraticaService praticaService;
	
	@PostMapping(value = "/{categoriaId}")
	public ResponseEntity<PraticaRetornoDTO> create(@Valid @RequestBody PraticaDTO praticaDto , @PathVariable Long categoriaId) {
		PraticaRetornoDTO created = praticaService.create(praticaDto, categoriaId);
		return ResponseEntity.status(HttpStatus.CREATED).body(created); 
	}
	
	@CrossOrigin
	@GetMapping()
	public ResponseEntity<List<PraticaRetornoDTO>> getAll() {
		return ResponseEntity.ok(praticaService.getAll());
	}
	
	@CrossOrigin
	@GetMapping(value = "/{praticaId}")
	public ResponseEntity<PraticaRetornoDTO> getOne(@PathVariable Long praticaId) {
		
		PraticaRetornoDTO dtoRetornado = praticaService.getOne(praticaId);
		
		if (dtoRetornado != null)
			return ResponseEntity.ok(dtoRetornado);
		
		throw new ResourceNotFoundException(praticaId);
	}
	
	@CrossOrigin
	@PutMapping(value = "/{praticaId}")
	public ResponseEntity<PraticaRetornoDTO> update(@PathVariable Long praticaId, @Valid @RequestBody PraticaDTO praticaDto) {
		PraticaRetornoDTO updated = praticaService.update(praticaId, praticaDto);
		return ResponseEntity.ok(updated);
	}
	
	@CrossOrigin
	@DeleteMapping(value = "/{praticaId}")
	public ResponseEntity<PraticaRetornoDTO> delete(@PathVariable Long praticaId) {
		PraticaRetornoDTO deleted = praticaService.delete(praticaId);
		return ResponseEntity.ok(deleted);
	}
}
