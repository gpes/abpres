package gpes.ifpb.edu.br.abpres.praticas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import org.hibernate.annotations.GenericGenerator;

import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;
import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;

@Entity
@Table(name = "TB_PRATICA")
public class Pratica implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id_pratica")
	private Long id;

	@NotBlank
	@Size(max = 100)
	private String titulo;


	@OneToMany(mappedBy = "pratica", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Arquivo> arquivos = new ArrayList<>();

	//Impede que ocorra uma recursividade inifinita na hora de printar o objeto (USADO PARA MANY TO MANY)
	@JsonIdentityInfo(
				generator = ObjectIdGenerators.PropertyGenerator.class,
				property = "id")

	//Jsonigore usado caso queira omitir a exibição do arraylist de categorias em uma pratica
	//@JsonIgnore
	//Parte não dominante do many to many
    @ManyToMany(mappedBy="praticas")
    private List<Categoria> categorias = new ArrayList<>();


	public Pratica() {
		
	}

	public Pratica(@NotBlank @Size(max = 100) String titulo, List<Arquivo> arquivos, List<Categoria> categorias) {
	
		this.titulo = titulo;
		this.arquivos = arquivos;
		this.categorias = categorias;
	}

	public Boolean addCat(Categoria c) {
		categorias.add(c);
		return true;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<Arquivo> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}

}
