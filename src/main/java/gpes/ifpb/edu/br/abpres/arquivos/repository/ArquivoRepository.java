package gpes.ifpb.edu.br.abpres.arquivos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;

public interface ArquivoRepository extends JpaRepository<Arquivo, Long> {

}
