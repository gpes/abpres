package gpes.ifpb.edu.br.abpres.exception;

import java.util.HashMap;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(annotations=RestController.class)
@SuppressWarnings({"rawtypes", "unchecked"})
public class ExceptionControllerHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity notFoundException(final ResourceNotFoundException e) {
		HashMap<String, String> map = new HashMap<>();
		map.put("Mensagem", "O recurso ["+e.getMessage()+"] não foi encontrado");
		return new ResponseEntity(map, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity badRequestException() {
		HashMap<String, String> map = new HashMap<>();
		map.put("Mensagem", "Existem parâmetros incorretos na solicitação");
		return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(DataIntegrityViolationException.class)
	public ResponseEntity conflictException() {
		HashMap<String, String> map = new HashMap<>();
		map.put("Mensagem", "O recurso solicitado já existe");
		return new ResponseEntity(map, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	public ResponseEntity unauthorizedException() {
		HashMap<String, String> map = new HashMap<>();
		map.put("Mensagem", "As credenciais enviadas estão incorretas");
		return new ResponseEntity(map, HttpStatus.UNAUTHORIZED);
	}
	
}
