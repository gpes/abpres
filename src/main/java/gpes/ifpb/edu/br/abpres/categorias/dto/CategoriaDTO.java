package gpes.ifpb.edu.br.abpres.categorias.dto;

import java.util.List;

import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;
import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;

public class CategoriaDTO {
	
	private String titulo;
	private List<Pratica> praticas;
	
	public CategoriaDTO() {
	}

	public CategoriaDTO(String titulo, List<Pratica> praticas) {
		this.titulo = titulo;
		this.praticas = praticas;
	}

	public CategoriaDTO(Categoria categoria) {
		
		this.titulo = categoria.getTitulo();
		this.praticas = categoria.getPraticas();
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public List<Pratica> getPraticas() {
		return praticas;
	}
	public void setPraticas(List<Pratica> praticas) {
		this.praticas = praticas;
	}
	
	public CategoriaDTO toCategoriaDto (Categoria categoria) {
		
		return new CategoriaDTO(categoria.getTitulo(),categoria.getPraticas());
	}
	
	public Categoria toCategoria (CategoriaDTO dtoCategoria) {
		return new Categoria(dtoCategoria.getTitulo(),dtoCategoria.getPraticas());
	}

}
