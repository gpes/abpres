package gpes.ifpb.edu.br.abpres.praticas.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import gpes.ifpb.edu.br.abpres.arquivos.dto.ArquivoDTO;
import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;
import gpes.ifpb.edu.br.abpres.arquivos.service.ArquivoService;
import gpes.ifpb.edu.br.abpres.categorias.dto.CategoriaDTO;
import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;
import gpes.ifpb.edu.br.abpres.categorias.service.CategoriaService;
import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;

public class PraticaRetornoDTO {
	
	private Long id;
	private String titulo;
	private List<ArquivoDTO> arquivos;
	private List<CategoriaDTO> categorias;

	@Autowired
	ArquivoService arquivoService;

	@Autowired
	CategoriaService categoriaService;
	
	public PraticaRetornoDTO() {
	
	}

	public PraticaRetornoDTO(Long id,String titulo, List<ArquivoDTO> arquivos, List<CategoriaDTO> categorias) {

		this.id = id;
		this.titulo = titulo;
		this.arquivos = arquivos;
		this.categorias = categorias;
	}

	public PraticaRetornoDTO(Pratica pratica){
		
		this.id = pratica.getId();
		this.titulo = pratica.getTitulo();

		this.arquivos = arquivoService.toArquivosDTOs(pratica.getArquivos());
		this.categorias = categoriaService.toCategoriaDTOs(pratica.getCategorias());

	}

	public PraticaRetornoDTO(PraticaDTO praticaDTO) {
	
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<ArquivoDTO> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<ArquivoDTO> arquivos) {
		this.arquivos = arquivos;
	}

	public List<CategoriaDTO> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<CategoriaDTO> categorias) {
		this.categorias = categorias;
	}
	
	public PraticaDTO toPraticaDTO (Pratica pratica) {
		
		List<ArquivoDTO> novosArquivosDto = new ArrayList<>();
		List<CategoriaDTO> novasCategoriasDto = new ArrayList<>();
		
		for(Arquivo arquivo : pratica.getArquivos()) {
			novosArquivosDto.add(new ArquivoDTO().toArquivoDto(arquivo));
		}
		
		for(Categoria categoria: pratica.getCategorias()) {
			novasCategoriasDto.add(new CategoriaDTO().toCategoriaDto(categoria));
		}
		
		return new PraticaDTO(
				pratica.getTitulo(),
				novosArquivosDto,
				novasCategoriasDto);
	}
	

	public Pratica toPratica (PraticaDTO praticaDto) {
		
		List<Arquivo> novosArquivos = new ArrayList<>();
		List<Categoria> novasCategorias = new ArrayList<>();
		
		for(ArquivoDTO arquivoDto : praticaDto.getArquivos()) {
			novosArquivos.add(arquivoDto.toArquivo(arquivoDto));
		}
		
		for(CategoriaDTO categoriaDto : praticaDto.getCategorias()) {
			novasCategorias.add(categoriaDto.toCategoria(categoriaDto));
		}
		
		return new Pratica(praticaDto.getTitulo(), novosArquivos, novasCategorias);
	}

	public Pratica toPratica (PraticaRetornoDTO praticaDto) {
		
		List<Arquivo> novosArquivos = new ArrayList<>();
		List<Categoria> novasCategorias = new ArrayList<>();
		
		for(ArquivoDTO arquivoDto : praticaDto.getArquivos()) {
			novosArquivos.add(arquivoDto.toArquivo(arquivoDto));
		}
		
		for(CategoriaDTO categoriaDto : praticaDto.getCategorias()) {
			novasCategorias.add(categoriaDto.toCategoria(categoriaDto));
		}
		
		return new Pratica(praticaDto.getTitulo(), novosArquivos, novasCategorias);
	}
}
