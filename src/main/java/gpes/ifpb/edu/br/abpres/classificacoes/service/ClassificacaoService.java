package gpes.ifpb.edu.br.abpres.classificacoes.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gpes.ifpb.edu.br.abpres.classificacoes.dto.ClassificacaoDTO;
import gpes.ifpb.edu.br.abpres.classificacoes.dto.ClassificacaoRetornoDTO;
import gpes.ifpb.edu.br.abpres.classificacoes.model.Classificacao;
import gpes.ifpb.edu.br.abpres.classificacoes.repository.ClassificacaoRepository;
import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;

@Service
public class ClassificacaoService {
	
	@Autowired
	private ClassificacaoRepository classificacaoRepository;
	
	public List<ClassificacaoRetornoDTO> getAll(){

		List<ClassificacaoRetornoDTO> listaDtos = new ArrayList<>();
		List<Classificacao> toConvert = classificacaoRepository.findAll();

		for (Classificacao classificacao : toConvert) {
			listaDtos.add(new ClassificacaoRetornoDTO(
				classificacao
			));
		}

		return listaDtos;
	}
	
	public ClassificacaoRetornoDTO getOne(Long classificacaoId) {

		Optional<Classificacao> classificacaoOpt = classificacaoRepository.findById(classificacaoId);

		if(classificacaoOpt.isEmpty()){
			throw new ResourceNotFoundException(classificacaoId);
		}

		return new ClassificacaoRetornoDTO(classificacaoOpt.get());
	}
	
	public ClassificacaoRetornoDTO create (ClassificacaoDTO classificacaoDto){
		
		Classificacao classificacao = classificacaoDto.toClassificacao(classificacaoDto);

		classificacaoRepository.save(classificacao);

		return new ClassificacaoRetornoDTO(classificacao);
	}

	public ClassificacaoRetornoDTO delete (Long classificacaoId) {
		Optional<Classificacao> classificacaoOpt = classificacaoRepository.findById(classificacaoId);
		
		if (!classificacaoOpt.isPresent()) {
			throw new ResourceNotFoundException(classificacaoId);
		}
		
		classificacaoRepository.deleteById(classificacaoId);
		return new ClassificacaoRetornoDTO(classificacaoOpt.get());
	}
	
	public ClassificacaoRetornoDTO update (Long classificacaoId, ClassificacaoDTO classificacaoDto) {
		
		Optional<Classificacao> classificacaoOpt = classificacaoRepository.findById(classificacaoId);
		
		Classificacao novaClassificacao = classificacaoDto.toClassificacao(classificacaoDto);

		if (classificacaoOpt.isPresent()) {
			novaClassificacao.setId(classificacaoId);
			classificacaoRepository.save(novaClassificacao);
			return new ClassificacaoRetornoDTO(novaClassificacao);
		}
		throw new ResourceNotFoundException(classificacaoId);
	}
	
}
