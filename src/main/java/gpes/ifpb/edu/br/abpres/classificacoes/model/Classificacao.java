package gpes.ifpb.edu.br.abpres.classificacoes.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;


@Entity
@Table(name = "TB_CLASSIFICACAO")
public class Classificacao implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator="native")
    @GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id_classificacao")
	private Long id;
	
	@NotBlank
	// De várias ocorrencias, coleta só uma para uma.
	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, optional = true)
	@JoinColumn(name = "arquivo_id", nullable = true)
	private Arquivo arquivo;
	
	@NotBlank
	@Size(max = 250)
	private String relevancia;
	
	private Long likes;
	
	private Long deslikes;

	public Classificacao() {
		
	}

	public Classificacao(@NotBlank Arquivo arquivo, @NotBlank @Size(max = 250) String relevancia, Long likes,
			Long deslikes) {
		super();
		this.arquivo = arquivo;
		this.relevancia = relevancia;
		this.likes = likes;
		this.deslikes = deslikes;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public String getRelevancia() {
		return relevancia;
	}

	public void setRelevancia(String relevancia) {
		this.relevancia = relevancia;
	}

	public Long getLikes() {
		return likes;
	}

	public void setLikes(Long likes) {
		this.likes = likes;
	}

	public Long getDeslikes() {
		return deslikes;
	}

	public void setDeslikes(Long deslikes) {
		this.deslikes = deslikes;
	}
	
	
	
	
}
