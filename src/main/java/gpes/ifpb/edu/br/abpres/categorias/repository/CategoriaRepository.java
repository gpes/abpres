package gpes.ifpb.edu.br.abpres.categorias.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
