package gpes.ifpb.edu.br.abpres.classificacoes.dto;

import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;
import gpes.ifpb.edu.br.abpres.classificacoes.model.Classificacao;

public class ClassificacaoRetornoDTO {

	private Long id;

	private String relevancia;
	
	private Arquivo arquivo;
	
	private Long likes;
	
	private Long deslikes;

	public ClassificacaoRetornoDTO() {
		
	}

	public ClassificacaoRetornoDTO(Long id, String relevancia, Arquivo arquivo, Long likes, Long deslikes) {
		
		this.id = id;
		this.relevancia = relevancia;
		this.arquivo = arquivo;
		this.likes = likes;
		this.deslikes = deslikes;
	}

	public ClassificacaoRetornoDTO(Classificacao classificacao) {

		this.id = classificacao.getId();
		this.relevancia = classificacao.getRelevancia();
		this.arquivo = classificacao.getArquivo();
		this.likes = classificacao.getLikes();
		this.deslikes = classificacao.getDeslikes();
	}

	public ClassificacaoRetornoDTO(ClassificacaoDTO classificacaoDTO) {
		
		this.relevancia = classificacaoDTO.getRelevancia();
		this.arquivo = classificacaoDTO.getArquivo();
		this.likes = classificacaoDTO.getLikes();
		this.deslikes = classificacaoDTO.getDeslikes();
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRelevancia() {
		return relevancia;
	}

	public void setRelevancia(String relevancia) {
		this.relevancia = relevancia;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public Long getLikes() {
		return likes;
	}

	public void setLikes(Long likes) {
		this.likes = likes;
	}

	public Long getDeslikes() {
		return deslikes;
	}

	public void setDeslikes(Long deslikes) {
		this.deslikes = deslikes;
	}

	public ClassificacaoRetornoDTO toClassificacaoDTO(Classificacao classificacao) {
		return new ClassificacaoRetornoDTO(classificacao.getId(),classificacao.getRelevancia(),classificacao.getArquivo(),
				classificacao.getLikes(),classificacao.getDeslikes());
	}
	
	public Classificacao toClassificacao(ClassificacaoRetornoDTO dtoClassificacao) {
		return new Classificacao(dtoClassificacao.getArquivo(),dtoClassificacao.getRelevancia(),
				dtoClassificacao.getLikes(),dtoClassificacao.getDeslikes());
	}
	
}
