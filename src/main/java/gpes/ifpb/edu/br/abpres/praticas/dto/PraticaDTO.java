package gpes.ifpb.edu.br.abpres.praticas.dto;

import java.util.ArrayList;
import java.util.List;

import gpes.ifpb.edu.br.abpres.arquivos.dto.ArquivoDTO;
import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;
import gpes.ifpb.edu.br.abpres.categorias.dto.CategoriaDTO;
import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;
import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;

public class PraticaDTO {
	
	private String titulo;
	private List<ArquivoDTO> arquivos;
	private List<CategoriaDTO> categorias;
	
	public PraticaDTO() {
	
	}

	public PraticaDTO(String titulo, List<ArquivoDTO> arquivos, List<CategoriaDTO> categorias) {

		this.titulo = titulo;
		this.arquivos = arquivos;
		this.categorias = categorias;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<ArquivoDTO> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<ArquivoDTO> arquivos) {
		this.arquivos = arquivos;
	}

	public List<CategoriaDTO> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<CategoriaDTO> categorias) {
		this.categorias = categorias;
	}
	
	public PraticaDTO toPraticaDTO (Pratica pratica) {
		
		List<ArquivoDTO> novosArquivosDto = new ArrayList<>();
		List<CategoriaDTO> novasCategoriasDto = new ArrayList<>();
		
		for(Arquivo arquivo : pratica.getArquivos()) {
			novosArquivosDto.add(new ArquivoDTO().toArquivoDto(arquivo));
		}
		
		for(Categoria categoria: pratica.getCategorias()) {
			novasCategoriasDto.add(new CategoriaDTO().toCategoriaDto(categoria));
		}
		
		return new PraticaDTO(
				pratica.getTitulo(),
				novosArquivosDto,
				novasCategoriasDto);
	}
	

	public Pratica toPratica (PraticaDTO praticaDto) {
		
		List<Arquivo> novosArquivos = new ArrayList<>();
		List<Categoria> novasCategorias = new ArrayList<>();
		
		for(ArquivoDTO arquivoDto : praticaDto.getArquivos()) {
			novosArquivos.add(arquivoDto.toArquivo(arquivoDto));
		}
		
		for(CategoriaDTO categoriaDto : praticaDto.getCategorias()) {
			novasCategorias.add(categoriaDto.toCategoria(categoriaDto));
		}
		
		return new Pratica(praticaDto.getTitulo(), novosArquivos, novasCategorias);
	}
}
