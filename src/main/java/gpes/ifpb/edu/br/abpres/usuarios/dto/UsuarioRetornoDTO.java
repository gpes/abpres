package gpes.ifpb.edu.br.abpres.usuarios.dto;

import org.springframework.beans.factory.annotation.Autowired;

import gpes.ifpb.edu.br.abpres.usuarios.model.Usuario;
import gpes.ifpb.edu.br.abpres.usuarios.repository.IUsuarioRepository;

public class UsuarioRetornoDTO {
	
	private Long id;
	private String username;
    private String email;
    private String tipo;
    
    @Autowired
    IUsuarioRepository usuarioRepository;
	
    public UsuarioRetornoDTO() {
		
	}

	public UsuarioRetornoDTO(Long id, String username, String email, String tipo) {

		this.id = id;
		this.username = username;
		this.email = email;
		this.tipo = tipo;
	}

	public UsuarioRetornoDTO(Usuario usuario) {
		
		this.id = usuario.getId();
		this.username = usuario.getUsername();
		this.email = usuario.getEmail();
		this.tipo = usuario.getTipo();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
    
	public UsuarioRetornoDTO toUsuarioRetornoDto(Usuario usuario) {
		return new UsuarioRetornoDTO(usuario.getId(),usuario.getUsername(),usuario.getEmail(),usuario.getTipo());
	}
    
	public UsuarioRetornoDTO toUsuarioRetornoDto(UsuarioDTO usuarioDto) {
		return new UsuarioRetornoDTO(null,usuarioDto.getUsername(),usuarioDto.getEmail(),usuarioDto.getTipo());
	}
    
}
