package gpes.ifpb.edu.br.abpres.usuarios.service;

import gpes.ifpb.edu.br.abpres.usuarios.model.AbpresUserDetails;
import gpes.ifpb.edu.br.abpres.usuarios.model.Usuario;
import gpes.ifpb.edu.br.abpres.usuarios.repository.IUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AbpresUserDetailsService implements UserDetailsService {

    @Autowired
    IUsuarioRepository repositorioUsuario;

    @Override
    public UserDetails loadUserByUsername(String nomeUsuario) throws UsernameNotFoundException {

        Optional<Usuario> usuario = repositorioUsuario.findByUsername(nomeUsuario);
        usuario.orElseThrow( () -> new UsernameNotFoundException("Usuário: \'" + nomeUsuario  + " \', Não encontrado) "));
        return usuario.map(AbpresUserDetails::new).get();
    }
}
