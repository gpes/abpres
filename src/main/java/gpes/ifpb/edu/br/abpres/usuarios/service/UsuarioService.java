package gpes.ifpb.edu.br.abpres.usuarios.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;
import gpes.ifpb.edu.br.abpres.exception.UnauthorizedException;
import gpes.ifpb.edu.br.abpres.usuarios.dto.UsuarioDTO;
import gpes.ifpb.edu.br.abpres.usuarios.dto.UsuarioRetornoDTO;

import gpes.ifpb.edu.br.abpres.usuarios.model.Usuario;
import gpes.ifpb.edu.br.abpres.usuarios.repository.IUsuarioRepository;
import gpes.ifpb.edu.br.abpres.usuarios.util.AccountCredentials;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UsuarioService {

	@Autowired
	private IUsuarioRepository usuarioRepository;

	@Autowired
	PasswordEncoder encoder;

	public Usuario autenticar(AccountCredentials ac) {
		Optional<Usuario> encontrado = usuarioRepository.findByUsernameOrEmail(ac.getLogin(), ac.getLogin());

		if (encontrado.isPresent()) {

			Usuario u = encontrado.get();
			
			if (encoder.matches(ac.getPassword(), u.getSenha())) {
				return u;
			}
		}
			
		throw new UnauthorizedException();
	}

	public Usuario create(UsuarioDTO dtoUsuario) {
		dtoUsuario.setSenha(encoder.encode(dtoUsuario.getSenha()));
		return usuarioRepository.save(dtoUsuario.toUsuario(dtoUsuario));
	}

	public List<UsuarioRetornoDTO> getAll() {
		

        List<UsuarioRetornoDTO> listaDtos = new ArrayList<>();
        List<Usuario> toCovert = usuarioRepository.findAll();

        for (Usuario usuario : toCovert) {
			listaDtos.add(
				new UsuarioRetornoDTO(
					usuario.getId(),
					usuario.getUsername(),
					usuario.getEmail(),
					usuario.getTipo() ) );
        }
        
        return listaDtos;
	}

	public UsuarioRetornoDTO getOne(Long usuarioId) {
		
		Optional<Usuario> usuarioOpcional = usuarioRepository.findById(usuarioId);
		
		if(usuarioOpcional.isEmpty()) {
			throw new ResourceNotFoundException(0L);
		}

		return new UsuarioRetornoDTO(usuarioOpcional.get());
	}

	public Usuario update(Long usuarioId, UsuarioDTO dtoUsuario) {
		Optional<Usuario> usuarioOpt = usuarioRepository.findById(usuarioId);

		if (usuarioOpt.isPresent()) {
			Usuario usuario = dtoUsuario.toUsuario(dtoUsuario);
			usuario.setId(usuarioId);
			return usuarioRepository.save(usuario);
		}

		throw new ResourceNotFoundException(usuarioId);
	}

	public Usuario delete(Long usuarioId) {
		Optional<Usuario> usuarioOpt = usuarioRepository.findById(usuarioId);

		if (!usuarioOpt.isPresent())
			throw new ResourceNotFoundException(usuarioId);

		usuarioRepository.deleteById(usuarioId);
		return usuarioOpt.get();
	}
}
