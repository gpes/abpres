package gpes.ifpb.edu.br.abpres.usuarios.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import gpes.ifpb.edu.br.abpres.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;
import gpes.ifpb.edu.br.abpres.usuarios.dto.UsuarioDTO;
import gpes.ifpb.edu.br.abpres.usuarios.dto.UsuarioRetornoDTO;
import gpes.ifpb.edu.br.abpres.usuarios.service.UsuarioService;
import gpes.ifpb.edu.br.abpres.usuarios.util.AccountCredentials;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {

	@Value("${abpres.setup.jwt.token-prefix}")
	private String tokenPrefix = "Bearer";

	@Value("${abpres.setup.jwt.secret}")
	private String secret = "AbpresSecret";

	@Value("${abpres.setup.jwt.header-string}")
	private String headerString = "Authorization";

	// @Value("${abpres.setup.jwt.expiration-time}")
	private Long expirationTime = 7200000L;
	
	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtTokenProvider tokenProvider;
	
	@CrossOrigin
	@PostMapping(value = "/autenticar")
	public ResponseEntity<UsuarioRetornoDTO> autenticarUsuario(HttpServletRequest request,
			HttpServletResponse response,
			@Valid @RequestBody AccountCredentials ac) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						ac.getLogin(),ac.getPassword()
				));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String token = tokenProvider.generateToken(authentication);

		response.addHeader(headerString, tokenPrefix + " " + token);
		response.setHeader("Access-Control-Expose-Headers", headerString);
		return ResponseEntity.ok(new UsuarioRetornoDTO(usuarioService.autenticar(ac)));
	}
	
	@CrossOrigin
	@PostMapping()
	public ResponseEntity<UsuarioRetornoDTO> create(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody UsuarioDTO dtoUsuario) {
		UsuarioRetornoDTO created = new UsuarioRetornoDTO(usuarioService.create(dtoUsuario) );
		return ResponseEntity.status(HttpStatus.CREATED).body(created); 
	}
	
	@CrossOrigin
	@GetMapping()
	public ResponseEntity<List<UsuarioRetornoDTO>> getAll() {
		return ResponseEntity.ok(usuarioService.getAll());
	}
	
	@CrossOrigin
	@GetMapping(value = "/{usuarioId}")
	public ResponseEntity<UsuarioRetornoDTO> getOne(@PathVariable Long usuarioId) {
		
		UsuarioRetornoDTO dtoRetorno = usuarioService.getOne(usuarioId);		
		
		if (dtoRetorno != null){
			return ResponseEntity.ok(dtoRetorno);
		}
		throw new ResourceNotFoundException(usuarioId);
	}
	
	@CrossOrigin
	@PutMapping(value = "/{usuarioId}")
	public ResponseEntity<UsuarioRetornoDTO> update(@PathVariable Long usuarioId, @Valid @RequestBody UsuarioDTO dtoUsuario) {
		UsuarioRetornoDTO updated = new UsuarioRetornoDTO(usuarioService.update(usuarioId, dtoUsuario));
		return ResponseEntity.ok(updated);
	}
	
	@CrossOrigin
	@DeleteMapping(value = "/{usuarioId}")
	public ResponseEntity<UsuarioRetornoDTO> delete(@PathVariable Long usuarioId) {
		UsuarioRetornoDTO deleted = new UsuarioRetornoDTO(usuarioService.delete(usuarioId));
		return ResponseEntity.ok(deleted);
	}
}
