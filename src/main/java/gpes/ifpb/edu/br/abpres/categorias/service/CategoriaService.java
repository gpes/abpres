package gpes.ifpb.edu.br.abpres.categorias.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gpes.ifpb.edu.br.abpres.categorias.dto.CategoriaDTO;
import gpes.ifpb.edu.br.abpres.categorias.dto.CategoriaRetornoDTO;
import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;
import gpes.ifpb.edu.br.abpres.categorias.repository.CategoriaRepository;

@Service
public class CategoriaService {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public CategoriaRetornoDTO create(CategoriaDTO categoriaDto) {
		
		Categoria categoria = categoriaDto.toCategoria(categoriaDto);
		
		categoriaRepository.save(categoria);
		
		return new CategoriaRetornoDTO(categoria);
	}
	
	public List<CategoriaRetornoDTO> getAll() {
		
		List<CategoriaRetornoDTO> listaDtos = new ArrayList<>();
		List<Categoria> toConvert = categoriaRepository.findAll();
		
		for(Categoria categoria: toConvert) {
			listaDtos.add(
					new CategoriaRetornoDTO(
						categoria
							)
					);
		}
		
		return listaDtos;
	}

	public CategoriaRetornoDTO getOne(Long categoriaId) {
		
		Optional<Categoria> categoria = categoriaRepository.findById(categoriaId);
		
		if(categoria.isEmpty()) {
			throw new ResourceNotFoundException(categoriaId);
		}
		
		return new CategoriaRetornoDTO(categoria.get());
	}

	public CategoriaRetornoDTO update(Long categoriaId, CategoriaDTO categoriaDto) {
		
		Optional<Categoria> categoriaOpt = categoriaRepository.findById(categoriaId);

		Categoria novaCategoria = categoriaDto.toCategoria(categoriaDto);
		
		if (categoriaOpt.isPresent()) {
			novaCategoria.setId(categoriaId);
			categoriaRepository.save(novaCategoria);
			return new CategoriaRetornoDTO(novaCategoria);
		}

		throw new ResourceNotFoundException(categoriaId);
	}

	public CategoriaRetornoDTO delete(Long categoriaId) {
		Optional<Categoria> categoriaOpt = categoriaRepository.findById(categoriaId);

		if (!categoriaOpt.isPresent())
			throw new ResourceNotFoundException(categoriaId);

		categoriaRepository.deleteById(categoriaId);
		return new CategoriaRetornoDTO(categoriaOpt.get());
	}

	public List<Categoria> toCategorias (List<CategoriaDTO> listaDtos){

		List<Categoria> listaCategorias = new ArrayList<>();

		for(CategoriaDTO categoriaDto : listaDtos){
			listaCategorias.add(
				categoriaDto.toCategoria(categoriaDto));
		}

		return listaCategorias;

	}

	public List<CategoriaDTO> toCategoriaDTOs (List<Categoria> categorias){

		List<CategoriaDTO> listaDtos = new ArrayList<>();

		for(Categoria categoria : categorias ){
			listaDtos.add(
				new CategoriaDTO(categoria)
			);
		}

		return listaDtos;

	}
}
