package gpes.ifpb.edu.br.abpres.usuarios.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import gpes.ifpb.edu.br.abpres.usuarios.model.Usuario;
import gpes.ifpb.edu.br.abpres.usuarios.repository.IUsuarioRepository;

public class UsuarioDTO {

    private String username;
    private String email;
    private String senha;
    private String tipo;

    @Autowired
    IUsuarioRepository usuarioRepository;

    public UsuarioDTO(String usuario, String email, String senha, String tipo){
        this.username = usuario;
        this.email = email;
        this.senha = senha;
        this.tipo = tipo;
    }

    public UsuarioDTO() {
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return this.senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public UsuarioDTO toUsuarioDTO(Usuario usuario){

        UsuarioDTO dtoRetornado = new UsuarioDTO();

        dtoRetornado.setUsername(usuario.getUsername());
        dtoRetornado.setEmail(usuario.getEmail());
        dtoRetornado.setSenha(usuario.getSenha());
        dtoRetornado.setTipo(usuario.getTipo());

        return dtoRetornado;
    }

    public Usuario toUsuario(UsuarioDTO usuarioDto){

        Usuario usuarioRetornado = new Usuario();

        usuarioRetornado.setUsername(usuarioDto.getUsername());
        usuarioRetornado.setEmail(usuarioDto.getEmail());
        usuarioRetornado.setSenha(usuarioDto.getSenha());
        usuarioRetornado.setTipo(usuarioDto.getTipo());

        return usuarioRetornado;
    }

    public List<UsuarioDTO> toUsuarioDTOs(){

        List<UsuarioDTO> listaDto = new ArrayList<>();
        List<Usuario> toCovert = usuarioRepository.findAll();

        for (Usuario usuario : toCovert) {
            listaDto.add(new UsuarioDTO(
                usuario.getUsername(),usuario.getEmail(),
                usuario.getSenha(),usuario.getTipo()));
        }
        
        return listaDto;
    }

}