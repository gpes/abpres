package gpes.ifpb.edu.br.abpres;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import gpes.ifpb.edu.br.abpres.usuarios.model.*;
import gpes.ifpb.edu.br.abpres.usuarios.repository.*;

@SpringBootApplication
public class AbpresApplication implements CommandLineRunner {

	@Autowired
	private Environment env;

	@Autowired
	private IUsuarioRepository usuarioRepository;

	public static void main(String[] args) {
		SpringApplication.run(AbpresApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		this.usuarioSeeder();

	}

	private void usuarioSeeder() {
		Optional<Usuario> u = usuarioRepository.findByUsernameOrEmail(
				env.getProperty("abpres.setup.admin.username"), env.getProperty("abpres.setup.admin.email"));

		if (!u.isPresent()) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

			Usuario toSave = new Usuario(env.getProperty("abpres.setup.admin.username"),
					env.getProperty("abpres.setup.admin.email"),
					encoder.encode(env.getProperty("abpres.setup.admin.password")),
					Tipo.ADMIN);
			
			usuarioRepository.save(toSave);
		}
	}

}
