package gpes.ifpb.edu.br.abpres.classificacoes.dto;

import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;
import gpes.ifpb.edu.br.abpres.classificacoes.model.Classificacao;

public class ClassificacaoDTO {

	
	private String relevancia;
	
	private Arquivo arquivo;
	
	private Long likes;
	
	private Long deslikes;

	public ClassificacaoDTO() {
		
	}

	public ClassificacaoDTO(String relevancia, Arquivo arquivo, Long likes, Long deslikes) {
		
		this.relevancia = relevancia;
		this.arquivo = arquivo;
		this.likes = likes;
		this.deslikes = deslikes;
	}

	public String getRelevancia() {
		return relevancia;
	}

	public void setRelevancia(String relevancia) {
		this.relevancia = relevancia;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public Long getLikes() {
		return likes;
	}

	public void setLikes(Long likes) {
		this.likes = likes;
	}

	public Long getDeslikes() {
		return deslikes;
	}

	public void setDeslikes(Long deslikes) {
		this.deslikes = deslikes;
	}

	public ClassificacaoDTO toClassificacaoDTO(Classificacao classificacao) {
		return new ClassificacaoDTO(classificacao.getRelevancia(),classificacao.getArquivo(),
				classificacao.getLikes(),classificacao.getDeslikes());
	}
	
	public Classificacao toClassificacao(ClassificacaoDTO dtoClassificacao) {
		return new Classificacao(dtoClassificacao.getArquivo(),dtoClassificacao.getRelevancia(),
				dtoClassificacao.getLikes(),dtoClassificacao.getDeslikes());
	}
	
}
