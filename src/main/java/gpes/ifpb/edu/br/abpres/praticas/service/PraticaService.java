package gpes.ifpb.edu.br.abpres.praticas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gpes.ifpb.edu.br.abpres.categorias.model.Categoria;
import gpes.ifpb.edu.br.abpres.categorias.repository.CategoriaRepository;
import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;
import gpes.ifpb.edu.br.abpres.praticas.repository.PraticaRepository;
import gpes.ifpb.edu.br.abpres.praticas.dto.PraticaDTO;
import gpes.ifpb.edu.br.abpres.praticas.dto.PraticaRetornoDTO;

@Service
public class PraticaService {
	
	@Autowired
	private PraticaRepository praticaRepository;
	@Autowired
	private CategoriaRepository categoriaRepository;

	
	public PraticaRetornoDTO create(PraticaDTO praticaDto, Long categoriaId) {

		Optional<Categoria> cat = categoriaRepository.findById(categoriaId);
		Pratica novaPratica = praticaDto.toPratica(praticaDto);

		if(cat.isPresent()){

			novaPratica.getCategorias().add(cat.get());
			cat.get().getPraticas().add(novaPratica); 
			novaPratica = praticaRepository.save(novaPratica);
			categoriaRepository.save(cat.get());
			return new PraticaRetornoDTO(novaPratica);
		}

		throw new ResourceNotFoundException(categoriaId);
	}
	
	public List<PraticaRetornoDTO> getAll() {

		List<PraticaRetornoDTO> listarDtos = new ArrayList<>();
		
		for (Pratica pratica : praticaRepository.findAll()) {
			listarDtos.add(new PraticaRetornoDTO(pratica));
		}

		return listarDtos;
	}

	public PraticaRetornoDTO getOne(Long praticaId) {

		Optional<Pratica> praticaOpt = praticaRepository.findById(praticaId);

		if(praticaOpt.isPresent()){
			return new PraticaRetornoDTO(praticaOpt.get());
		}

		throw new ResourceNotFoundException(praticaId);

	}

	public PraticaRetornoDTO update(Long praticaId, PraticaDTO praticaDto) {
		
		Optional<Pratica> praticaOpt = praticaRepository.findById(praticaId);
		Pratica novaPratica = praticaDto.toPratica(praticaDto);

		if (praticaOpt.isPresent()) {
			novaPratica.setId(praticaId);
			praticaRepository.save(novaPratica);
			return new PraticaRetornoDTO(novaPratica);
		}

		throw new ResourceNotFoundException(praticaId);
	}

	public PraticaRetornoDTO delete(Long praticaId) {
		Optional<Pratica> praticaOpt = praticaRepository.findById(praticaId);


		if (!praticaOpt.isPresent())
			throw new ResourceNotFoundException(praticaId);
		
		for (Categoria c : praticaOpt.get().getCategorias()) {
			c.getPraticas().remove(praticaOpt.get());
			categoriaRepository.save(c);
		}
		praticaOpt.get().getCategorias().clear();
		praticaRepository.deleteById(praticaId);
		return new PraticaRetornoDTO(praticaOpt.get());

	}
}
	