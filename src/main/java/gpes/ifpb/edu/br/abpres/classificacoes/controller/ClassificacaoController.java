package gpes.ifpb.edu.br.abpres.classificacoes.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gpes.ifpb.edu.br.abpres.classificacoes.dto.ClassificacaoDTO;
import gpes.ifpb.edu.br.abpres.classificacoes.dto.ClassificacaoRetornoDTO;
import gpes.ifpb.edu.br.abpres.classificacoes.service.ClassificacaoService;
import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/classificacoes")
public class ClassificacaoController {
	
	@Autowired
	private ClassificacaoService classificacaoService;
	
	@GetMapping
	public ResponseEntity<List<ClassificacaoRetornoDTO>> getAll(){
		return ResponseEntity.ok(classificacaoService.getAll());
	}
	
	@GetMapping(value = "/{classificacaoId}")
	public ResponseEntity<ClassificacaoRetornoDTO> getOne(@PathVariable Long classificacaoId){
		
		ClassificacaoRetornoDTO classifcacaoDto = classificacaoService.getOne(classificacaoId);
		
		if(classificacaoId != null) {
			return ResponseEntity.ok(classifcacaoDto);
		}
		
		throw new ResourceNotFoundException(classificacaoId);
	}
	
	@PostMapping(value = "/{classificacaoId}")
	public ResponseEntity<ClassificacaoRetornoDTO> create(@Valid @RequestBody ClassificacaoDTO classificacaoDto){
		
		ClassificacaoRetornoDTO created = classificacaoService.create(classificacaoDto);

		return ResponseEntity.status(HttpStatus.CREATED).body(created);
	}

	@PutMapping(value = "/{classificacaoId}")
	public ResponseEntity<ClassificacaoRetornoDTO> update(@PathVariable Long classificacaoId, @Valid @RequestBody ClassificacaoDTO classificacaoDto){
		
		ClassificacaoRetornoDTO atualizado = classificacaoService.update(classificacaoId, classificacaoDto);
		
		if (atualizado == null) {
				throw new ResourceNotFoundException(classificacaoId);
		}
		return ResponseEntity.ok(atualizado);
	}
	
	@DeleteMapping(value = "/{classificacaoId}")
	public ResponseEntity<ClassificacaoRetornoDTO> delete(@PathVariable Long classificacaoId){
		ClassificacaoRetornoDTO removido = classificacaoService.delete(classificacaoId);
		
		if (removido == null) {
			throw new ResourceNotFoundException(classificacaoId);
		}
		
		return ResponseEntity.ok(removido);
	}

}
