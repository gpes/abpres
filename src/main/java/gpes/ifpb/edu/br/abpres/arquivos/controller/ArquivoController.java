package gpes.ifpb.edu.br.abpres.arquivos.controller;

import java.util.List;

import javax.validation.Valid;

import gpes.ifpb.edu.br.abpres.arquivos.service.ArquivoService;
import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;
import gpes.ifpb.edu.br.abpres.praticas.service.PraticaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import gpes.ifpb.edu.br.abpres.arquivos.dto.ArquivoDTO;
import gpes.ifpb.edu.br.abpres.arquivos.dto.ArquivoRetornoDTO;
import gpes.ifpb.edu.br.abpres.praticas.dto.PraticaDTO;
import gpes.ifpb.edu.br.abpres.praticas.dto.PraticaRetornoDTO;
import gpes.ifpb.edu.br.abpres.praticas.model.Pratica;

@RestController
@RequestMapping("/api/arquivos")
public class ArquivoController {

    @Autowired
    private ArquivoService arquivoService;
// IMPLEMENTAR UM METODO PRA DOWNLOAD QUE ESSE METODO APENAS LISTA
    //	@Autowired
    private PraticaService praticaService;

//	@GetMapping(value = "/{arquivoId}/download")
//	public ResponseEntity<Arquivo> downloadArquivo(@PathVariable Long arquivoId) {
//		Arquivo arquivo = arquivoService.download(arquivoId);
//		return ResponseEntity.ok(arquivo);
//	}

    @GetMapping()
    public ResponseEntity<List<ArquivoRetornoDTO>> getAll() {
        return ResponseEntity.ok(arquivoService.getAll());
    }

    @GetMapping(value = "/{arquivoId}")
    public ResponseEntity<ArquivoRetornoDTO> getOne(@PathVariable Long arquivoId) {
        ArquivoRetornoDTO arquivoDto = arquivoService.getOne(arquivoId);

        if (arquivoDto != null)
            return ResponseEntity.ok(arquivoDto);

        throw new ResourceNotFoundException(arquivoId);
    }

    @PutMapping(value = "/{arquivoId}")
    public ResponseEntity<ArquivoRetornoDTO> update(@PathVariable Long arquivoId, @Valid @RequestBody ArquivoDTO arquivoDto) {
        
        ArquivoRetornoDTO updatedDto = arquivoService.update(arquivoId, arquivoDto);
        
        if(updatedDto == null) {
			throw new ResourceNotFoundException(arquivoId);
        }
        return ResponseEntity.ok(updatedDto);
    }

    @DeleteMapping(value = "/{arquivoId}")
    public ResponseEntity<ArquivoRetornoDTO> delete(@PathVariable Long arquivoId) {
        ArquivoRetornoDTO deleted = arquivoService.delete(arquivoId);
        return ResponseEntity.ok(deleted);
    }

    @PostMapping("/adicionar")
    public ResponseEntity<ArquivoRetornoDTO> add(@Valid @ModelAttribute ArquivoDTO arquivoDto, @RequestParam Long praticaId,
        @RequestParam MultipartFile file) {

        PraticaRetornoDTO praticaDto = praticaService.getOne(praticaId);

        System.out.println(arquivoDto);

        if (praticaDto != null){
            var path = arquivoService.uploadFile(file);
            Pratica p = praticaDto.toPratica(praticaDto);
            arquivoDto.setPratica(p);
            arquivoDto.setPath(path);
            p.getArquivos().add(arquivoDto.toArquivo(arquivoDto));
            praticaService.update(p.getId(), new PraticaDTO().toPraticaDTO(p));
            ArquivoRetornoDTO created = arquivoService.create(arquivoDto);
            return ResponseEntity.ok(created);
        }

        throw new ResourceNotFoundException(praticaId);
    }

}
