package gpes.ifpb.edu.br.abpres.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import gpes.ifpb.edu.br.abpres.classificacoes.model.Classificacao;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException(Long id) {
		super("id - "+id);
	}

	public ResourceNotFoundException(Classificacao classificacao) {
		super("Classificação não encontrada" + classificacao.toString());
	}
}
