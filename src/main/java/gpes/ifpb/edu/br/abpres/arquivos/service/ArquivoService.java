package gpes.ifpb.edu.br.abpres.arquivos.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;

import gpes.ifpb.edu.br.abpres.exception.ResourceNotFoundException;
import gpes.ifpb.edu.br.abpres.exception.StorageException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import gpes.ifpb.edu.br.abpres.arquivos.dto.ArquivoDTO;
import gpes.ifpb.edu.br.abpres.arquivos.dto.ArquivoRetornoDTO;
import gpes.ifpb.edu.br.abpres.arquivos.model.Arquivo;
import gpes.ifpb.edu.br.abpres.arquivos.repository.ArquivoRepository;

@Service
public class ArquivoService {

	@Autowired
	private ArquivoRepository arquivoRepository;

	@Autowired
	ServletContext context;

	public ArquivoRetornoDTO download(Long arquivoId) {
		
		return null;
	}
	
	public List<ArquivoRetornoDTO> getAll() {

		List<ArquivoRetornoDTO> listaDtos = new ArrayList<>();
		List<Arquivo> toConvert = arquivoRepository.findAll();

		for (Arquivo arquivo : toConvert) {
			listaDtos.add(new ArquivoRetornoDTO(
				arquivo
			));
		}

		return listaDtos;
	}

	public ArquivoRetornoDTO getOne(Long arquivoId) {

		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if(arquivoOpt.isEmpty()){
			throw new ResourceNotFoundException(arquivoId);
		}

		return new ArquivoRetornoDTO(arquivoOpt.get());
	}

	public ArquivoRetornoDTO update(Long arquivoId, ArquivoDTO arquivoDto) {
		
		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		Arquivo novoArquivo = arquivoDto.toArquivo(arquivoDto);

		if (arquivoOpt.isPresent()) {
			novoArquivo.setId(arquivoId);
			novoArquivo = arquivoRepository.save(novoArquivo);
			return new ArquivoRetornoDTO(novoArquivo);
		}

		throw new ResourceNotFoundException(arquivoId);
	}

	public ArquivoRetornoDTO delete(Long arquivoId) {

		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if (!arquivoOpt.isPresent())
			throw new ResourceNotFoundException(arquivoId);

		arquivoRepository.deleteById(arquivoId);
		return new ArquivoRetornoDTO(arquivoOpt.get());
	}

	public ArquivoRetornoDTO create(ArquivoDTO arquivoDto) {

		Arquivo novoArquivo = arquivoDto.toArquivo(arquivoDto);

		arquivoRepository.save(novoArquivo);

		return new ArquivoRetornoDTO(novoArquivo);
	}

	public String uploadFile(MultipartFile file) {

		if (file.isEmpty()) {
			throw new StorageException("Failed to store empty file");
		}

		try {
			byte[] bytes = file.getBytes();
			Path path = Paths.get(context.getRealPath("uploads") + file.getOriginalFilename());
			Files.write(path, bytes);
			return Paths.get(context.getRealPath("uploads") + file.getOriginalFilename()).toString();
		} catch (IOException e) {

			var msg = String.format("Failed to store file %s", file.getName());

			throw new StorageException(msg, e);
		}

	}

	public List<Arquivo> toArquivos(List<ArquivoDTO> listaDtos){
		
		List<Arquivo> listaArquivos = new ArrayList<>();

		for (ArquivoDTO arquivoDto : listaDtos){
			listaArquivos.add(
					arquivoDto.toArquivo(arquivoDto)
			);
		}

		return listaArquivos;
	}

	public List<ArquivoDTO> toArquivosDTOs(List<Arquivo> arquivos){

		List<ArquivoDTO> listaDtos = new ArrayList<>();

		for (Arquivo arquivo: arquivos){
			listaDtos.add(
				new ArquivoDTO(arquivo)
			);
		}

		return listaDtos;

	}

}
