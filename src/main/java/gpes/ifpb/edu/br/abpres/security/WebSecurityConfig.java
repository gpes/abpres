package gpes.ifpb.edu.br.abpres.security;

import gpes.ifpb.edu.br.abpres.usuarios.model.Tipo;
import gpes.ifpb.edu.br.abpres.usuarios.service.AbpresUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String ENDPOINT_USUARIOS = "/api/usuarios";
	private static final String ENDPOINT_CATEGORIAS = "/api/categorias";
	private static final String ENDPOINT_PRATICAS = "/api/praticas";
	private static final String USUARIO_ADMIN = Tipo.ADMIN.name();
	private static final String USUARIO_BASICO = Tipo.BASICO.name();

	@Autowired
	AbpresUserDetailsService abpresUserDetailsService;

//	@Autowired
//	private JwtAuthenticationEntryPoint unauthorizedHandler;

//	@Bean
//	public JwtAuthenticationFilter jwtAuthenticationFilter() {
//		return new JwtAuthenticationFilter();
//	}

	@Bean
	public PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(abpresUserDetailsService).passwordEncoder(passwordEncoder());
		auth.authenticationProvider(daoAuthenticationProvider());
	}

	@Bean
	public DaoAuthenticationProvider daoAuthenticationProvider(){
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(abpresUserDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {		
		httpSecurity.csrf().disable().authorizeRequests()
		.antMatchers(HttpMethod.POST, 		ENDPOINT_USUARIOS + "/autenticar").permitAll()
		.antMatchers(HttpMethod.GET, 		ENDPOINT_USUARIOS).hasAuthority(USUARIO_ADMIN)
		.antMatchers(HttpMethod.POST, 		ENDPOINT_USUARIOS + "/**").hasAuthority(USUARIO_ADMIN)
		.antMatchers(HttpMethod.PUT, 		ENDPOINT_USUARIOS + "/**").hasAnyAuthority(USUARIO_ADMIN,USUARIO_BASICO)
		.antMatchers(HttpMethod.DELETE,		ENDPOINT_USUARIOS + "/**").hasAuthority(USUARIO_ADMIN)
		.antMatchers(HttpMethod.GET, 		ENDPOINT_CATEGORIAS).hasAnyAuthority(USUARIO_ADMIN,USUARIO_BASICO)
		.antMatchers(HttpMethod.POST, 		ENDPOINT_CATEGORIAS + "/**").hasAnyAuthority(USUARIO_ADMIN,USUARIO_BASICO)
		.antMatchers(HttpMethod.GET, 		ENDPOINT_PRATICAS).hasAnyAuthority(USUARIO_ADMIN,USUARIO_BASICO)
		.antMatchers(HttpMethod.POST, 		ENDPOINT_PRATICAS + "/**").hasAnyAuthority(USUARIO_ADMIN,USUARIO_BASICO)
		.antMatchers(HttpMethod.PUT, 		ENDPOINT_PRATICAS + "/**").hasAnyAuthority(USUARIO_ADMIN,USUARIO_BASICO)
		.antMatchers(HttpMethod.DELETE,		ENDPOINT_PRATICAS + "/**").hasAnyAuthority(USUARIO_ADMIN,USUARIO_BASICO)
		.anyRequest().authenticated()
		.and()

				.addFilterBefore(new JwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS);
	}


}
