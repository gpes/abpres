package gpes.ifpb.edu.br.abpres.usuarios.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AbpresUserDetails implements UserDetails {

    private String nomeUsuario;
    private String senhaUsuario;
    private boolean estadoUsuario;
    private List<GrantedAuthority> tipos;

    public AbpresUserDetails(Usuario usuario){
        this.nomeUsuario = usuario.getUsername();
        this.senhaUsuario = usuario.getSenha();
        this.estadoUsuario = true;
        this.tipos = Arrays.stream(usuario.getTipo().split(",") ).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.tipos;
    }

    @Override
    public String getPassword() {
        return this.senhaUsuario;
    }

    @Override
    public String getUsername() {
        return this.nomeUsuario;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.estadoUsuario;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.estadoUsuario;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.estadoUsuario;
    }

    @Override
    public boolean isEnabled() {
        return this.estadoUsuario;
    }
}
