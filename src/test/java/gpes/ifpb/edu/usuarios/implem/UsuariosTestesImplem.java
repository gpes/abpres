package gpes.ifpb.edu.usuarios.implem;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import gpes.ifpb.edu.usuarios.IUsuariosTestes;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;


public class UsuariosTestesImplem implements IUsuariosTestes {

    private String tokenAdmin;
    private Integer idUsuario;

    public void setTokenAdmin(String tokenAdmin) {
        this.tokenAdmin = tokenAdmin;
    }

    public void setIdUsuario(Integer idUsuario){
        this.idUsuario = idUsuario;
    }

    public String getTokenAdmin(){
        return this.tokenAdmin;
    }

    public Integer getIdUsuario(){
        return this.idUsuario;
    }

    @Override
    @BeforeTest
    public void autenticarAdmin() {
        
        Response resposta =
        given()
            .contentType(ContentType.JSON)
            .body(ADMIN_TEST_USER)
        .when()
            .post(ABPRES_URI + "api/usuarios/autenticar");

        setTokenAdmin( resposta.getHeader("Authorization"));
    }

    @Override
    @BeforeMethod
    public void inserirUsuario() {

        Response requisicao = 
        given()
            .header("Authorization",tokenAdmin)
            .contentType(ContentType.JSON)
            .body(
                "{\"username\" : \"xico\", \"email\" : \"xico@ifpb\", \"senha\" : \"xico123\", \"tipo\" : \"ROLE_BASICO\"  }"
            )
        .when()
            .post(ABPRES_URI + "api/usuarios");

        setIdUsuario(requisicao.jsonPath().getInt("id"));

        requisicao.then()
            .assertThat()
            .statusCode(201);
    }

    @Override @Test
    public void listarUsuario() {

            Response requisicao =
                when()
                    .get(ABPRES_URI + "api/usuarios");

            requisicao.then()
                .assertThat()
                .statusCode(200);

        }

    @Override @Test
    public void atualizarUsuario() {

        Response requisicao = 
        given()
            .header("Authorization",tokenAdmin)
            .pathParams("id", getIdUsuario())
            .contentType(ContentType.JSON)
            .body(
                "{\"username\" : \"Xico Silva\", \"email\" : \"xico@mail.edu\", \"senha\" : \"xico1234\", \"tipo\" : \"ROLE_BASICO\"  }"
            )
        .when()
            .put(ABPRES_URI + "api/usuarios/{id}");

        requisicao.then()
            .assertThat()
            .statusCode(200);

    }

    @Override @Test
    public void listarUmUsuario() {
        Response requisicao =
        given()
        .header("Authorization",tokenAdmin)
        .pathParams("id", getIdUsuario())
        .when()
            .get(ABPRES_URI + "api/usuarios/{id}");

        requisicao.then()
            .assertThat()
            .statusCode(200);

    }


    @Override @Test
    public void removerUsuario() {

        Response requisicao = 
        given()
            .header("Authorization",tokenAdmin)
            .pathParams("id", getIdUsuario())
        .when()
            .delete(ABPRES_URI + "api/usuarios/{id}");

        requisicao.then()
            .assertThat()
            .statusCode(200);

    }


    
}