package gpes.ifpb.edu.usuarios;

public interface IUsuariosTestes {

    public final String ABPRES_URI = "http://localhost:57000/";
    public final String ADMIN_TEST_USER = "{ \"login\": \"Administrador\", \"password\": \"admin123\" }";
    
    public void autenticarAdmin();
    public void inserirUsuario();
    public void listarUsuario();
    public void listarUmUsuario();
    public void atualizarUsuario();
    public void removerUsuario();

}