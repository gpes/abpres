package gpes.ifpb.edu.br.abpres;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("testes")
class AbpresApplicationTests {

	@Test
	void contextLoads() {
		
	}
}
